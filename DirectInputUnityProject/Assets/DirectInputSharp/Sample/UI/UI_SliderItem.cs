﻿using UnityEngine;
using UnityEngine.UI;

public class UI_SliderItem : MonoBehaviour
{
    [SerializeField] private Image _backgroundImage;

    [SerializeField] private Text _indexText;
    [SerializeField] private Text _valueText;

    public void Fill(int index, float value = 0)
    {
        _indexText.text = $"SLIDER {index}";
        _valueText.text = value.ToString("0.00");
        UpdateBackgroundColor(value);
    }

    public void SetValue(float value)
    {
        _valueText.text = value.ToString("0.00");
        UpdateBackgroundColor(value);
    }

    private void UpdateBackgroundColor(float value)
    {
        _backgroundImage.color = Color.Lerp(UI_ColorPallete.DeactivatedColor, UI_ColorPallete.ActivatedColor, Mathf.Abs(value));
    }
}
