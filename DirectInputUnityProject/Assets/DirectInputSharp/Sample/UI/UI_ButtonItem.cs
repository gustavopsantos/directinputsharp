﻿using UnityEngine;
using UnityEngine.UI;

public class UI_ButtonItem : MonoBehaviour
{
    [SerializeField] private Image _backgroundImage;

    [SerializeField] private Text _indexText;
    [SerializeField] private Text _valueText;

    public void Fill(int index, bool value = false)
    {
        _indexText.text = $"BUTTON {index}";
        _valueText.text = value.ToString();
        UpdateBackgroundColor(value);
    }

    public void SetValue(bool value)
    {
        _valueText.text = value.ToString();
        UpdateBackgroundColor(value);
    }

    private void UpdateBackgroundColor(bool value)
    {
        _backgroundImage.color = value ? UI_ColorPallete.ActivatedColor : UI_ColorPallete.DeactivatedColor;
    }
}
