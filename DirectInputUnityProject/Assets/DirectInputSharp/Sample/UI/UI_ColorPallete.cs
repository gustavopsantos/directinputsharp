﻿using UnityEngine;

public static class UI_ColorPallete
{
    public static Color ActivatedColor => new Color(0.84f, 0.49f, 0.01f);
    public static Color DeactivatedColor => new Color(0.15f, 0.15f, 0.15f);
}