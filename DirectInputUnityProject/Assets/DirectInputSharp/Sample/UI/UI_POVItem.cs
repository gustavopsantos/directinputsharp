﻿using UnityEngine;
using UnityEngine.UI;

public class UI_POVItem : MonoBehaviour
{
    [SerializeField] private Image _backgroundImage;

    [SerializeField] private Text _indexText;
    [SerializeField] private Text _valueText;

    public void Fill(int index, string value = "None")
    {
        _indexText.text = $"POV {index}";
        _valueText.text = value;
        UpdateBackgroundColor(value);
    }

    public void SetValue(string value)
    {
        _valueText.text = value;
        UpdateBackgroundColor(value);
    }

    private void UpdateBackgroundColor(string value)
    {
        _backgroundImage.color = value != "None" ? UI_ColorPallete.ActivatedColor : UI_ColorPallete.DeactivatedColor;
    }
}
