﻿using UnityEngine;
using UnityEngine.UI;
using DirectInputSharp;
using System.Collections.Generic;

public class UI_JoystickView : MonoBehaviour
{
    public const int POVCount = 4;
    public const int AxisCount = 24;
    public const int SliderCount = 8;
    public const int ButtonCount = 128;
    
    public int Index;

    [Header("First Line")] public Text Name;

    [Header("Second Line")] public Text IndexText;
    public Text ConnectedText;
    public Text ButtonsCountText;
    public Text AxisCountText;
    public Text POVsCountText;

    [Header("Prefabs")] public UI_AxisItem AxisItemPrefab;
    public UI_ButtonItem ButtonItemPrefab;
    public UI_POVItem POVItemPrefab;
    public UI_SliderItem SliderItemPrefab;

    [Header("Holders")] public Transform AxisItemsHolder;
    public Transform ButtonItemsHolder;
    public Transform POVItemsHolder;
    public Transform SliderItemsHolder;

    //------------------------
    //Private Variables
    //------------------------
    private readonly List<UI_AxisItem> _axisItems = new List<UI_AxisItem>();
    private readonly List<UI_ButtonItem> _buttonItems = new List<UI_ButtonItem>();
    private readonly List<UI_POVItem> _povItems = new List<UI_POVItem>();
    private readonly List<UI_SliderItem> _sliderItems = new List<UI_SliderItem>();

    private DirectInputSharp.Joystick _joystick;

    private void Start()
    {
        if (DirectInputSharp.Manager.Instance.Joysticks == null ||
            DirectInputSharp.Manager.Instance.Joysticks.Count <= 0)
        {
            gameObject.SetActive(false);
            return;
        }

        if (Index > DirectInputSharp.Manager.Instance.Joysticks.Count - 1)
        {
            gameObject.SetActive(false);
            return;
        }

        _joystick = DirectInputSharp.Manager.Instance.Joysticks[Index];

        if (_joystick == null)
        {
            gameObject.SetActive(false);
            return;
        }
        
        InstantiateAxisItems();
        InstantiateButtonItems();
        InstantiatePOVItems();
        InstantiateSliderItems();
    }

    private void Update()
    {
        if (_joystick == null)
        {
            return;
        }

        Name.text = _joystick.Name;
        IndexText.text = Index.ToString();
        ConnectedText.text = _joystick.IsConnected ? "YES" : "NO";
        ButtonsCountText.text = _joystick.ButtonsCount.ToString();
        AxisCountText.text = _joystick.AxisCount.ToString();
        POVsCountText.text = _joystick.POVsCount.ToString();

        //Update Axis
        for (int i = 0; i < AxisCount; i++)
        {
            if (!_axisItems[i].gameObject.activeSelf && Mathf.Abs(_joystick.GetAxisDelta(i)) > 0 )
                _axisItems[i].gameObject.SetActive(true);
            
            _axisItems[i].SetValue(_joystick.GetAxis(i));
        }

        //Update Buttons
        for (int i = 0; i < ButtonCount; i++)
        {
            if (!_buttonItems[i].gameObject.activeSelf && _joystick.GetButtonDown(i))
                _buttonItems[i].gameObject.SetActive(true);
            
            _buttonItems[i].SetValue(_joystick.GetButton(i));
        }

        //Update POVs
        for (int i = 0; i < POVCount; i++)
        {
            if (!_povItems[i].gameObject.activeSelf && _joystick.GetPOV(i) != POVButton.None)
                _povItems[i].gameObject.SetActive(true);
            
            if (_joystick.GetPOVButton(POVButton.Up, i))
                _povItems[i].SetValue("Up");
            else if (_joystick.GetPOVButton(POVButton.UpRight, i))
                _povItems[i].SetValue("Up + Right");
            else if (_joystick.GetPOVButton(POVButton.Right, i))
                _povItems[i].SetValue("Right");
            else if (_joystick.GetPOVButton(POVButton.RightDown, i))
                _povItems[i].SetValue("Right + Down");
            else if (_joystick.GetPOVButton(POVButton.Down, i))
                _povItems[i].SetValue("Down");
            else if (_joystick.GetPOVButton(POVButton.DownLeft, i))
                _povItems[i].SetValue("Down + Left");
            else if (_joystick.GetPOVButton(POVButton.Left, i))
                _povItems[i].SetValue("Left");
            else if (_joystick.GetPOVButton(POVButton.LeftUp, i))
                _povItems[i].SetValue("Left + Up");
            else
                _povItems[i].SetValue("None");
        }

        //Update Slider
        for (int i = 0; i < SliderCount; i++)
        {
            if (!_sliderItems[i].gameObject.activeSelf && Mathf.Abs(_joystick.GetSliderDelta(i)) > 0 )
                _sliderItems[i].gameObject.SetActive(true);
            
            _sliderItems[i].SetValue(_joystick.GetSlider(i));
        }
    }

    //------------------------
    //Private Methods
    //------------------------
    private void InstantiateAxisItems()
    {
        for (int i = 0; i < AxisCount; i++)
        {
            UI_AxisItem axisItem = Instantiate(AxisItemPrefab, AxisItemsHolder);
            axisItem.Fill(i);
            axisItem.gameObject.SetActive(false);
            _axisItems.Add(axisItem);
        }
    }

    private void InstantiateButtonItems()
    {
        for (int i = 0; i < ButtonCount; i++)
        {
            UI_ButtonItem buttonItem = Instantiate(ButtonItemPrefab, ButtonItemsHolder);
            buttonItem.Fill(i);
            buttonItem.gameObject.SetActive(false);
            _buttonItems.Add(buttonItem);
        }
    }

    private void InstantiatePOVItems()
    {
        for (int i = 0; i < POVCount; i++)
        {
            UI_POVItem povItem = Instantiate(POVItemPrefab, POVItemsHolder);
            povItem.Fill(i);
            povItem.gameObject.SetActive(false);
            _povItems.Add(povItem);
        }
    }

    private void InstantiateSliderItems()
    {
        for (int i = 0; i < SliderCount; i++)
        {
            UI_SliderItem sliderItem = Instantiate(SliderItemPrefab, SliderItemsHolder);
            sliderItem.Fill(i);
            sliderItem.gameObject.SetActive(false);
            _sliderItems.Add(sliderItem);
        }
    }
}