using System;
using System.Runtime.InteropServices;

namespace DirectInputSharp
{
    internal static class Extensions
    {
        internal static string FromCharPointerToString(this IntPtr p)
        {
            string s = Marshal.PtrToStringAnsi(p);
            Marshal.FreeHGlobal(p);
            return s;
        }
    }
}