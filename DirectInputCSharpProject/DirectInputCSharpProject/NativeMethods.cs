using System;
using System.Runtime.InteropServices;

namespace DirectInputSharp
{
    internal static class NativeMethods
    {
        public const string x86 = "DirectInputSharp_32";
        public const string x64 = "DirectInputSharp_64";

        #region Exposed

        //-------------------------------------------------------------------------------------------------------------------------------
        //General
        //-------------------------------------------------------------------------------------------------------------------------------
        public static IntPtr Initialize()
        {
            if (Environment.Is64BitProcess)
                return Initialize64();
            return Initialize32();
        }

        public static void DeleteInputHandler(IntPtr inputHandler)
        {
            if (Environment.Is64BitProcess)
                DeleteInputHandler64(inputHandler);
            DeleteInputHandler32(inputHandler);
        }

        public static IntPtr GetJoystick(IntPtr inputHandler, int index)
        {
            if (Environment.Is64BitProcess)
                return GetJoystick64(inputHandler, index);
            return GetJoystick32(inputHandler, index);
        }

        //-------------------------------------------------------------------------------------------------------------------------------
        //Properties
        //-------------------------------------------------------------------------------------------------------------------------------
        public static int GetConnectedJoysticksCount(IntPtr inputHandler)
        {
            if (Environment.Is64BitProcess)
                return GetConnectedJoysticksCount64(inputHandler);
            return GetConnectedJoysticksCount32(inputHandler);
        }

        public static IntPtr GetJoystickName(IntPtr joystick)
        {
            if (Environment.Is64BitProcess)
                return GetJoystickName64(joystick);
            return GetJoystickName32(joystick);
        }

        public static int GetJoystickButtonsCount(IntPtr joystick)
        {
            if (Environment.Is64BitProcess)
                return GetJoystickButtonsCount64(joystick);
            return GetJoystickButtonsCount32(joystick);
        }

        public static int GetJoystickAxisCount(IntPtr joystick)
        {
            if (Environment.Is64BitProcess)
                return GetJoystickAxisCount64(joystick);
            return GetJoystickAxisCount32(joystick);
        }

        public static int GetJoystickPOVsCount(IntPtr joystick)
        {
            if (Environment.Is64BitProcess)
                return GetJoystickPOVsCount64(joystick);
            return GetJoystickPOVsCount32(joystick);
        }

        //-------------------------------------------------------------------------------------------------------------------------------
        //Polling
        //-------------------------------------------------------------------------------------------------------------------------------
        public static bool PollJoystick(IntPtr joystick)
        {
            if (Environment.Is64BitProcess)
                return PollJoystick64(joystick);
            return PollJoystick32(joystick);
        }

        //-------------------------------------------------------------------------------------------------------------------------------
        //Buttons
        //-------------------------------------------------------------------------------------------------------------------------------
        public static bool GetButton(IntPtr joystick, int button)
        {
            if (Environment.Is64BitProcess)
                return GetButton64(joystick, button);
            return GetButton32(joystick, button);
        }

        public static bool GetButtonDown(IntPtr joystick, int button)
        {
            if (Environment.Is64BitProcess)
                return GetButtonDown64(joystick, button);
            return GetButtonDown32(joystick, button);
        }

        public static bool GetButtonStay(IntPtr joystick, int button)
        {
            if (Environment.Is64BitProcess)
                return GetButtonStay64(joystick, button);
            return GetButtonStay32(joystick, button);
        }

        public static bool GetButtonUp(IntPtr joystick, int button)
        {
            if (Environment.Is64BitProcess)
                return GetButtonUp64(joystick, button);
            return GetButtonUp32(joystick, button);
        }

        //-------------------------------------------------------------------------------------------------------------------------------
        //Axis
        //-------------------------------------------------------------------------------------------------------------------------------
        public static void SetAxisDeadzone(IntPtr joystick, int axis, float deadzone)
        {
            if (Environment.Is64BitProcess)
                SetAxisDeadzone64(joystick, axis, deadzone);
            SetAxisDeadzone32(joystick, axis, deadzone);
        }

        public static float GetAxis(IntPtr joystick, int axis)
        {
            if (Environment.Is64BitProcess)
                return GetAxis64(joystick, axis);
            return GetAxis32(joystick, axis);
        }

        public static float GetLastAxis(IntPtr joystick, int axis)
        {
            if (Environment.Is64BitProcess)
                return GetLastAxis64(joystick, axis);
            return GetLastAxis32(joystick, axis);
        }

        public static float GetAxisDelta(IntPtr joystick, int axis)
        {
            if (Environment.Is64BitProcess)
                return GetAxisDelta64(joystick, axis);
            return GetAxisDelta32(joystick, axis);
        }

        //-------------------------------------------------------------------------------------------------------------------------------
        //POVs
        //-------------------------------------------------------------------------------------------------------------------------------
        public static int GetPOV(IntPtr joystick, int povIndex)
        {
            if (Environment.Is64BitProcess)
                return GetPOV64(joystick, povIndex);
            return GetPOV32(joystick, povIndex);
        }

        public static bool GetPOVButton(IntPtr joystick, int button, int povIndex)
        {
            if (Environment.Is64BitProcess)
                return GetPOVButton64(joystick, button, povIndex);
            return GetPOVButton32(joystick, button, povIndex);
        }

        public static bool GetPOVButtonDown(IntPtr joystick, int button, int povIndex)
        {
            if (Environment.Is64BitProcess)
                return GetPOVButtonDown64(joystick, button, povIndex);
            return GetPOVButtonDown32(joystick, button, povIndex);
        }

        public static bool GetPOVButtonStay(IntPtr joystick, int button, int povIndex)
        {
            if (Environment.Is64BitProcess)
                return GetPOVButtonStay64(joystick, button, povIndex);
            return GetPOVButtonStay32(joystick, button, povIndex);
        }

        public static bool GetPOVButtonUp(IntPtr joystick, int button, int povIndex)
        {
            if (Environment.Is64BitProcess)
                return GetPOVButtonUp64(joystick, button, povIndex);
            return GetPOVButtonUp32(joystick, button, povIndex);
        }

        //-------------------------------------------------------------------------------------------------------------------------------
        //Sliders
        //-------------------------------------------------------------------------------------------------------------------------------
        public static void SetSliderDeadzone(IntPtr joystick, int slider, float deadzone)
        {
            if (Environment.Is64BitProcess)
                SetSliderDeadzone64(joystick, slider, deadzone);
            SetSliderDeadzone32(joystick, slider, deadzone);
        }

        public static float GetSlider(IntPtr joystick, int slider)
        {
            if (Environment.Is64BitProcess)
                return GetSlider64(joystick, slider);
            return GetSlider32(joystick, slider);
        }

        public static float GetLastSlider(IntPtr joystick, int slider)
        {
            if (Environment.Is64BitProcess)
                return GetLastSlider64(joystick, slider);
            return GetLastSlider32(joystick, slider);
        }

        public static float GetSliderDelta(IntPtr joystick, int slider)
        {
            if (Environment.Is64BitProcess)
                return GetSliderDelta64(joystick, slider);
            return GetSliderDelta32(joystick, slider);
        }

        #endregion

        #region x86

        //-------------------------------------------------------------------------------------------------------------------------------
        //General
        //-------------------------------------------------------------------------------------------------------------------------------
        [DllImport(x86, EntryPoint = "Initialize")]
        private static extern IntPtr Initialize32();

        [DllImport(x86, EntryPoint = "DeleteInputHandler")]
        private static extern void DeleteInputHandler32(IntPtr inputHandler);

        [DllImport(x86, EntryPoint = "GetJoystick")]
        private static extern IntPtr GetJoystick32(IntPtr inputHandler, int index);

        //-------------------------------------------------------------------------------------------------------------------------------
        //Properties
        //-------------------------------------------------------------------------------------------------------------------------------
        [DllImport(x86, EntryPoint = "GetConnectedJoysticksCount")]
        private static extern int GetConnectedJoysticksCount32(IntPtr inputHandler);

        [DllImport(x86, EntryPoint = "GetJoystickName")]
        private static extern IntPtr GetJoystickName32(IntPtr joystick);

        [DllImport(x86, EntryPoint = "GetJoystickButtonsCount")]
        private static extern int GetJoystickButtonsCount32(IntPtr joystick);

        [DllImport(x86, EntryPoint = "GetJoystickAxisCount")]
        private static extern int GetJoystickAxisCount32(IntPtr joystick);

        [DllImport(x86, EntryPoint = "GetJoystickPOVsCount")]
        private static extern int GetJoystickPOVsCount32(IntPtr joystick);

        //-------------------------------------------------------------------------------------------------------------------------------
        //Polling
        //-------------------------------------------------------------------------------------------------------------------------------
        [DllImport(x86, EntryPoint = "PollJoystick")]
        private static extern bool PollJoystick32(IntPtr joystick);

        //-------------------------------------------------------------------------------------------------------------------------------
        //Buttons
        //-------------------------------------------------------------------------------------------------------------------------------
        [DllImport(x86, EntryPoint = "GetButton")]
        private static extern bool GetButton32(IntPtr joystick, int button);

        [DllImport(x86, EntryPoint = "GetButtonDown")]
        private static extern bool GetButtonDown32(IntPtr joystick, int button);

        [DllImport(x86, EntryPoint = "GetButtonStay")]
        private static extern bool GetButtonStay32(IntPtr joystick, int button);

        [DllImport(x86, EntryPoint = "GetButtonUp")]
        private static extern bool GetButtonUp32(IntPtr joystick, int button);

        //-------------------------------------------------------------------------------------------------------------------------------
        //Axis
        //-------------------------------------------------------------------------------------------------------------------------------
        [DllImport(x86, EntryPoint = "SetAxisDeadzone")]
        private static extern void SetAxisDeadzone32(IntPtr joystick, int axis, float deadzone);

        [DllImport(x86, EntryPoint = "GetAxis")]
        private static extern float GetAxis32(IntPtr joystick, int axis);

        [DllImport(x86, EntryPoint = "GetLastAxis")]
        private static extern float GetLastAxis32(IntPtr joystick, int axis);

        [DllImport(x86, EntryPoint = "GetAxisDelta")]
        private static extern float GetAxisDelta32(IntPtr joystick, int axis);

        //-------------------------------------------------------------------------------------------------------------------------------
        //POVs
        //-------------------------------------------------------------------------------------------------------------------------------
        [DllImport(x86, EntryPoint = "GetPOV")]
        private static extern int GetPOV32(IntPtr joystick, int povIndex);

        [DllImport(x86, EntryPoint = "GetPOVButton")]
        private static extern bool GetPOVButton32(IntPtr joystick, int button, int povIndex);

        [DllImport(x86, EntryPoint = "GetPOVButtonDown")]
        private static extern bool GetPOVButtonDown32(IntPtr joystick, int button, int povIndex);

        [DllImport(x86, EntryPoint = "GetPOVButtonStay")]
        private static extern bool GetPOVButtonStay32(IntPtr joystick, int button, int povIndex);

        [DllImport(x86, EntryPoint = "GetPOVButtonUp")]
        private static extern bool GetPOVButtonUp32(IntPtr joystick, int button, int povIndex);

        //-------------------------------------------------------------------------------------------------------------------------------
        //Sliders
        //-------------------------------------------------------------------------------------------------------------------------------
        [DllImport(x86, EntryPoint = "SetSliderDeadzone")]
        private static extern void SetSliderDeadzone32(IntPtr joystick, int slider, float deadzone);

        [DllImport(x86, EntryPoint = "GetSlider")]
        private static extern float GetSlider32(IntPtr joystick, int slider);

        [DllImport(x86, EntryPoint = "GetLastSlider")]
        private static extern float GetLastSlider32(IntPtr joystick, int slider);

        [DllImport(x86, EntryPoint = "GetSliderDelta")]
        private static extern float GetSliderDelta32(IntPtr joystick, int slider);

        #endregion

        #region x64

        //-------------------------------------------------------------------------------------------------------------------------------
        //General
        //-------------------------------------------------------------------------------------------------------------------------------
        [DllImport(x64, EntryPoint = "Initialize")]
        private static extern IntPtr Initialize64();

        [DllImport(x64, EntryPoint = "DeleteInputHandler")]
        private static extern void DeleteInputHandler64(IntPtr inputHandler);

        [DllImport(x64, EntryPoint = "GetJoystick")]
        private static extern IntPtr GetJoystick64(IntPtr inputHandler, int index);

        //-------------------------------------------------------------------------------------------------------------------------------
        //Properties
        //-------------------------------------------------------------------------------------------------------------------------------
        [DllImport(x64, EntryPoint = "GetConnectedJoysticksCount")]
        private static extern int GetConnectedJoysticksCount64(IntPtr inputHandler);

        [DllImport(x64, EntryPoint = "GetJoystickName")]
        private static extern IntPtr GetJoystickName64(IntPtr joystick);

        [DllImport(x64, EntryPoint = "GetJoystickButtonsCount")]
        private static extern int GetJoystickButtonsCount64(IntPtr joystick);

        [DllImport(x64, EntryPoint = "GetJoystickAxisCount")]
        private static extern int GetJoystickAxisCount64(IntPtr joystick);

        [DllImport(x64, EntryPoint = "GetJoystickPOVsCount")]
        private static extern int GetJoystickPOVsCount64(IntPtr joystick);

        //-------------------------------------------------------------------------------------------------------------------------------
        //Polling
        //-------------------------------------------------------------------------------------------------------------------------------
        [DllImport(x64, EntryPoint = "PollJoystick")]
        private static extern bool PollJoystick64(IntPtr joystick);

        //-------------------------------------------------------------------------------------------------------------------------------
        //Buttons
        //-------------------------------------------------------------------------------------------------------------------------------
        [DllImport(x64, EntryPoint = "GetButton")]
        private static extern bool GetButton64(IntPtr joystick, int button);

        [DllImport(x64, EntryPoint = "GetButtonDown")]
        private static extern bool GetButtonDown64(IntPtr joystick, int button);

        [DllImport(x64, EntryPoint = "GetButtonStay")]
        private static extern bool GetButtonStay64(IntPtr joystick, int button);

        [DllImport(x64, EntryPoint = "GetButtonUp")]
        private static extern bool GetButtonUp64(IntPtr joystick, int button);

        //-------------------------------------------------------------------------------------------------------------------------------
        //Axis
        //-------------------------------------------------------------------------------------------------------------------------------
        [DllImport(x64, EntryPoint = "SetAxisDeadzone")]
        private static extern void SetAxisDeadzone64(IntPtr joystick, int axis, float deadzone);

        [DllImport(x64, EntryPoint = "GetAxis")]
        private static extern float GetAxis64(IntPtr joystick, int axis);

        [DllImport(x64, EntryPoint = "GetLastAxis")]
        private static extern float GetLastAxis64(IntPtr joystick, int axis);

        [DllImport(x64, EntryPoint = "GetAxisDelta")]
        private static extern float GetAxisDelta64(IntPtr joystick, int axis);

        //-------------------------------------------------------------------------------------------------------------------------------
        //POVs
        //-------------------------------------------------------------------------------------------------------------------------------
        [DllImport(x64, EntryPoint = "GetPOV")]
        private static extern int GetPOV64(IntPtr joystick, int povIndex);

        [DllImport(x64, EntryPoint = "GetPOVButton")]
        private static extern bool GetPOVButton64(IntPtr joystick, int button, int povIndex);

        [DllImport(x64, EntryPoint = "GetPOVButtonDown")]
        private static extern bool GetPOVButtonDown64(IntPtr joystick, int button, int povIndex);

        [DllImport(x64, EntryPoint = "GetPOVButtonStay")]
        private static extern bool GetPOVButtonStay64(IntPtr joystick, int button, int povIndex);

        [DllImport(x64, EntryPoint = "GetPOVButtonUp")]
        private static extern bool GetPOVButtonUp64(IntPtr joystick, int button, int povIndex);

        //-------------------------------------------------------------------------------------------------------------------------------
        //Sliders
        //-------------------------------------------------------------------------------------------------------------------------------
        [DllImport(x64, EntryPoint = "SetSliderDeadzone")]
        private static extern void SetSliderDeadzone64(IntPtr joystick, int slider, float deadzone);

        [DllImport(x64, EntryPoint = "GetSlider")]
        private static extern float GetSlider64(IntPtr joystick, int slider);

        [DllImport(x64, EntryPoint = "GetLastSlider")]
        private static extern float GetLastSlider64(IntPtr joystick, int slider);

        [DllImport(x64, EntryPoint = "GetSliderDelta")]
        private static extern float GetSliderDelta64(IntPtr joystick, int slider);

        #endregion
    }
}