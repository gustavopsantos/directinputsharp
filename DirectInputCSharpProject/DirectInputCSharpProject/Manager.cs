namespace DirectInputSharp
{
    using System;
    using UnityEngine;
    using System.Collections.Generic;

    [DefaultExecutionOrder(int.MinValue)]
    public class Manager : MonoBehaviour
    {
        public static Manager Instance
        {
            get
            {
                if (_instance == null)
                {
                    GameObject directInputManager = new GameObject("[DirectInputSharpManager]");
                    _instance = directInputManager.AddComponent<Manager>();
                    directInputManager.isStatic = true;
                    DontDestroyOnLoad(directInputManager);
                }

                return _instance;
            }
        }

        private static Manager _instance;

        public List<Joystick> Joysticks = new List<Joystick>();
        private IntPtr _inputHandler;

        private void OnEnable()
        {
            _inputHandler = NativeMethods.Initialize();

            int connectedJoysticksCount = NativeMethods.GetConnectedJoysticksCount(_inputHandler);

            for (int i = 0; i < connectedJoysticksCount; i++)
                Joysticks.Add(new Joystick(NativeMethods.GetJoystick(_inputHandler, i)));
        }

        private void Update()
        {
            foreach (Joystick joystick in Joysticks)
                joystick.Poll();
        }
    }
}