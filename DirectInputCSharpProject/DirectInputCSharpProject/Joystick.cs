using System;
using UnityEngine;

namespace DirectInputSharp
{
    public enum POVButton
    {
        None = -1,
        Up = 0,
        UpRight = 1,
        Right = 2,
        RightDown = 3,
        Down = 4,
        DownLeft = 5,
        Left = 6,
        LeftUp = 7
    }

    [Serializable]
    public class Joystick
    {
        public Joystick(IntPtr pointer)
        {
            _pointer = pointer;
            _isConnected = NativeMethods.PollJoystick(_pointer);
            Name = NativeMethods.GetJoystickName(_pointer).FromCharPointerToString();
            ButtonsCount = NativeMethods.GetJoystickButtonsCount(_pointer);
            AxisCount = NativeMethods.GetJoystickAxisCount(_pointer);
            POVsCount = NativeMethods.GetJoystickPOVsCount(_pointer);
        }

        private readonly IntPtr _pointer;

        public string Name;
        public int AxisCount;
        public int POVsCount;
        public int ButtonsCount;
        public bool IsConnected => _isConnected;
        [SerializeField] private bool _isConnected;

        //-----------------------------------------------------------------------------------------------------------------------------
        // Internal Methods
        //-----------------------------------------------------------------------------------------------------------------------------
        internal void Poll()                                      { _isConnected = NativeMethods.PollJoystick(_pointer);               }

        //-----------------------------------------------------------------------------------------------------------------------------
        // Axis Methods
        //-----------------------------------------------------------------------------------------------------------------------------
        public void SetAxisDeadzone   (int axis, float deadzone)  { NativeMethods.SetAxisDeadzone(_pointer, axis, deadzone);           }
        public float GetAxis          (int axis)                  { return NativeMethods.GetAxis(_pointer, axis);                      }
        public float GetLastAxis      (int axis)                  { return NativeMethods.GetLastAxis(_pointer, axis);                  }
        public float GetAxisDelta     (int axis)                  { return NativeMethods.GetAxisDelta(_pointer, axis);                 }

        //-----------------------------------------------------------------------------------------------------------------------------
        // Button Methods
        //-----------------------------------------------------------------------------------------------------------------------------
        public bool GetButton         (int button)                { return NativeMethods.GetButton(_pointer, button);                  }
        public bool GetButtonDown     (int button)                { return NativeMethods.GetButtonDown(_pointer, button);              }
        public bool GetButtonStay     (int button)                { return NativeMethods.GetButtonStay(_pointer, button);              }
        public bool GetButtonUp       (int button)                { return NativeMethods.GetButtonUp(_pointer, button);                }

        //-----------------------------------------------------------------------------------------------------------------------------
        // POV Methods
        //-----------------------------------------------------------------------------------------------------------------------------
        public POVButton GetPOV      (int pov)                    { return (POVButton)NativeMethods.GetPOV(_pointer, pov);             }
        public bool GetPOVButton     (POVButton button, int pov)  { return NativeMethods.GetPOVButton(_pointer, (int)button, pov);     }
        public bool GetPOVButtonDown (POVButton button, int pov)  { return NativeMethods.GetPOVButtonDown(_pointer, (int)button, pov); }
        public bool GetPOVButtonStay (POVButton button, int pov)  { return NativeMethods.GetPOVButtonStay(_pointer, (int)button, pov); }
        public bool GetPOVButtonUp   (POVButton button, int pov)  { return NativeMethods.GetPOVButtonUp(_pointer, (int)button, pov);   }

        //-----------------------------------------------------------------------------------------------------------------------------
        // Slider Methods
        //-----------------------------------------------------------------------------------------------------------------------------
        public void SetSliderDeadzone (int slider, float deadzone) { NativeMethods.SetSliderDeadzone(_pointer, slider, deadzone);      }
        public float GetSlider      (int slider)                   { return NativeMethods.GetSlider(_pointer, slider);                 }
        public float GetLastSlider  (int slider)                   { return NativeMethods.GetLastSlider(_pointer, slider);             }
        public float GetSliderDelta (int slider)                   { return NativeMethods.GetSliderDelta(_pointer, slider);            }
    }
}