#include "InputHandler.h"

HWND InputHandler::GetConsoleHwnd(void)
{
#define MY_BUFSIZE 1024 // Buffer size for console window titles.
	HWND hwndFound;         // This is what is returned to the caller.
	char pszNewWindowTitle[MY_BUFSIZE]; // Contains fabricated
										// WindowTitle.
	char pszOldWindowTitle[MY_BUFSIZE]; // Contains original
										// WindowTitle.

	// Fetch current window title.

	GetConsoleTitle(pszOldWindowTitle, MY_BUFSIZE);

	// Format a "unique" NewWindowTitle.

	wsprintf(pszNewWindowTitle, "%d/%d", GetTickCount64(), GetCurrentProcessId());

	// Change current window title.

	SetConsoleTitle(pszNewWindowTitle);

	// Ensure window title has been updated.

	Sleep(40);

	// Look for NewWindowTitle.

	hwndFound = FindWindow(NULL, pszNewWindowTitle);

	// Restore original window title.

	SetConsoleTitle(pszOldWindowTitle);

	return(hwndFound);
}

bool InputHandler::Initialize()
{
	HRESULT hr;
	hr = DirectInput8Create(GetModuleHandle(nullptr), DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)& directInput, NULL);
	if (FAILED(hr))
		return false;

	hr = directInput->EnumDevices(DI8DEVCLASS_GAMECTRL, &staticEnumerateGameControllers, this, DIEDFL_ATTACHEDONLY);
	if (FAILED(hr))
		return false;

	for (LPDIRECTINPUTDEVICE8 gameController : gameControllers)
	{
		if (!InitializeJoystick(gameController))
		{
			return false;
		}
	}

	return true;
}


bool InputHandler::InitializeJoystick(LPDIRECTINPUTDEVICE8 gameController)
{
	HRESULT hr;

	hr = gameController->SetCooperativeLevel(GetConsoleHwnd(), DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
	if (FAILED(hr))
	{
		return false;
	}

	hr = gameController->SetDataFormat(&c_dfDIJoystick);
	if (FAILED(hr))
	{
		return false;
	}

	hr = gameController->EnumObjects(&staticSetGameControllerAxesRanges, gameController, DIDFT_AXIS);
	if (FAILED(hr))
	{
		return false;
	}

	hr = gameController->EnumObjects(&staticSetGameControllerAxesRanges, gameController, DIDFT_FFACTUATOR);
	if (FAILED(hr))
	{
		return false;
	}

	hr = gameController->Acquire();
	if (FAILED(hr))
		return false;

	joysticks.push_back(Joystick(gameController));

	return true;
}

BOOL CALLBACK InputHandler::staticSetGameControllerAxesRanges(LPCDIDEVICEOBJECTINSTANCE devObjInst, LPVOID pvRef)
{
	// the game controller
	LPDIRECTINPUTDEVICE8 gameController = (LPDIRECTINPUTDEVICE8)pvRef;
	gameController->Unacquire();

	// structure to hold game controller range properties
	DIPROPRANGE gameControllerRange;

	// setting range
	gameControllerRange.lMin = -1000;
	gameControllerRange.lMax = 1000;

	// set the size of the structure
	gameControllerRange.diph.dwSize = sizeof(DIPROPRANGE);
	gameControllerRange.diph.dwHeaderSize = sizeof(DIPROPHEADER);

	// set the object that we want to change		
	gameControllerRange.diph.dwHow = DIPH_BYID;
	gameControllerRange.diph.dwObj = devObjInst->dwType;

	// now set the range for the axis		
	if (FAILED(gameController->SetProperty(DIPROP_RANGE, &gameControllerRange.diph))) {
		return DIENUM_STOP;
	}

	//--------------------------------------------------------------------------------------
	//DEADZONE
	//--------------------------------------------------------------------------------------
	//DIPROPDWORD gameControllerDeadZone;
	//gameControllerDeadZone.dwData = 10;

	//// set the size of the structure
	//gameControllerDeadZone.diph.dwSize = sizeof(DIPROPDWORD);
	//gameControllerDeadZone.diph.dwHeaderSize = sizeof(DIPROPHEADER);

	//// set the object that we want to change
	//gameControllerDeadZone.diph.dwHow = DIPH_BYID;
	//gameControllerDeadZone.diph.dwObj = devObjInst->dwType;

	//// now set the dead zone for the axis
	//if (FAILED(gameController->SetProperty(DIPROP_DEADZONE, &gameControllerDeadZone.diph)))
	//	return DIENUM_STOP;

	return DIENUM_CONTINUE;
}


InputHandler::InputHandler()
{
}


InputHandler::~InputHandler()
{
}

BOOL CALLBACK InputHandler::staticEnumerateGameControllers(LPCDIDEVICEINSTANCE devInst, LPVOID pvRef)
{
	InputHandler* inputHandlerInstance = (InputHandler*)pvRef;
	return inputHandlerInstance->enumerateGameControllers(devInst);
}

BOOL InputHandler::enumerateGameControllers(LPCDIDEVICEINSTANCE devInst)
{
	// enumerate devices
	LPDIRECTINPUTDEVICE8 gameController;

	// create interface for the current game controller
	if (FAILED(directInput->CreateDevice(devInst->guidInstance, &gameController, NULL)))
		return DIENUM_CONTINUE;
	else
	{
		// store the game controller
		gameControllers.push_back(gameController); // (std::make_pair<std::wstring, LPDIRECTINPUTDEVICE8>(devInst->tszProductName, std::move(gameController)));
		return DIENUM_CONTINUE;
	}
}