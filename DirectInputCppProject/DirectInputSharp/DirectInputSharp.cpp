#include "DirectInputSharp.h"

DIRECT_INPUT_SHARP_API InputHandler* Initialize()
{
	InputHandler* inputHandler = new InputHandler();

	if (inputHandler->Initialize())
		return inputHandler;
	else
		return nullptr;
}

DIRECT_INPUT_SHARP_API void DeleteInputHandler(InputHandler* inputHandler)
{
	delete inputHandler;
}

DIRECT_INPUT_SHARP_API int GetConnectedJoysticksCount(InputHandler* inputHandler)
{
	return inputHandler->joysticks.size();
}

DIRECT_INPUT_SHARP_API Joystick* GetJoystick(InputHandler* inputHandler, int index)
{
	return &inputHandler->joysticks[index];
}

DIRECT_INPUT_SHARP_API char* GetJoystickName(Joystick* joystick)
{
	return _strdup(joystick->Name.c_str());
}

DIRECT_INPUT_SHARP_API int GetJoystickButtonsCount(Joystick* joystick)
{
	return joystick->numberOfButtons;
}

DIRECT_INPUT_SHARP_API int GetJoystickAxisCount(Joystick* joystick)
{
	return joystick->numberOfAxes;
}
DIRECT_INPUT_SHARP_API int GetJoystickPOVsCount(Joystick* joystick)
{
	return joystick->numberOfPOVs;
}

//Polling
DIRECT_INPUT_SHARP_API bool PollJoystick(Joystick* joystick)
{
	return joystick->Poll();
}

//Buttons
DIRECT_INPUT_SHARP_API bool GetButton(Joystick* joystick, int button)
{
	return joystick->GetButton(button);
}

DIRECT_INPUT_SHARP_API bool GetButtonDown(Joystick* joystick, int button)
{
	return joystick->GetButtonDown(button);
}

DIRECT_INPUT_SHARP_API bool GetButtonStay(Joystick* joystick, int button)
{
	return joystick->GetButtonStay(button);
}

DIRECT_INPUT_SHARP_API bool GetButtonUp(Joystick* joystick, int button)
{
	return joystick->GetButtonUp(button);
}

//Axis
DIRECT_INPUT_SHARP_API void SetAxisDeadzone(Joystick* joystick, int axis, float deadzone)
{
	joystick->AxisDeadzones[axis] = deadzone;
}
DIRECT_INPUT_SHARP_API float GetAxis(Joystick* joystick, int axis)
{
	return joystick->GetAxis(axis);
}
DIRECT_INPUT_SHARP_API float GetLastAxis(Joystick* joystick, int axis)
{
	return joystick->GetLastAxis(axis);
}
DIRECT_INPUT_SHARP_API float GetAxisDelta(Joystick* joystick, int axis)
{
	return joystick->GetAxisDelta(axis);
}

//POVs
DIRECT_INPUT_SHARP_API int GetPOV(Joystick* joystick, int povIndex)
{
	return joystick->GetPOV(povIndex);
}

DIRECT_INPUT_SHARP_API bool GetPOVButton(Joystick* joystick, int button, int povIndex)
{
	return joystick->GetPOVButton(button, povIndex);
}

DIRECT_INPUT_SHARP_API bool GetPOVButtonDown(Joystick* joystick, int button, int povIndex)
{
	return joystick->GetPOVButtonDown(button, povIndex);
}

DIRECT_INPUT_SHARP_API bool GetPOVButtonStay(Joystick* joystick, int button, int povIndex)
{
	return joystick->GetPOVButtonStay(button, povIndex);
}

DIRECT_INPUT_SHARP_API bool GetPOVButtonUp(Joystick* joystick, int button, int povIndex)
{
	return joystick->GetPOVButtonUp(button, povIndex);
}

//Sliders
DIRECT_INPUT_SHARP_API void SetSliderDeadzone(Joystick* joystick, int slider, float deadzone)
{
	joystick->SlidersDeadzones[slider] = deadzone;
}
DIRECT_INPUT_SHARP_API float GetSlider(Joystick* joystick, int slider)
{
	return joystick->GetSlider(slider);
}
DIRECT_INPUT_SHARP_API float GetLastSlider(Joystick* joystick, int slider)
{
	return joystick->GetLastSlider(slider);
}
DIRECT_INPUT_SHARP_API float GetSliderDelta(Joystick* joystick, int slider)
{
	return joystick->GetSliderDelta(slider);
}