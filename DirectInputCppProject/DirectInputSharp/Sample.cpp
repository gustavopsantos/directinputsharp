#define DESIRED_FPS 60
#define SLEEP_TIME 1000.0f / DESIRED_FPS
#define FIRST_JOYSTICK inputHandler.joysticks[0]

#include <iostream>
#include <thread>
#include <chrono>

#include "InputHandler.h"

int main()
{
	InputHandler inputHandler = InputHandler();

	if (!inputHandler.Initialize())
		return -1;

	//Setting up axis deadzones

	FIRST_JOYSTICK.AxisDeadzones[0] = 0.02f; //2%
	FIRST_JOYSTICK.AxisDeadzones[1] = 0.02f; //2%
	FIRST_JOYSTICK.AxisDeadzones[3] = 0.02f; //2%
	FIRST_JOYSTICK.AxisDeadzones[2] = 0.02f; //2%

	//Setting up sliders deadzones
	FIRST_JOYSTICK.SlidersDeadzones[0] = 0.02f; //2%
	FIRST_JOYSTICK.SlidersDeadzones[1] = 0.02f; //2%
	FIRST_JOYSTICK.SlidersDeadzones[3] = 0.02f; //2%
	FIRST_JOYSTICK.SlidersDeadzones[2] = 0.02f; //2%

	while (true)
	{
		if (inputHandler.joysticks.size() > 0)
		{
			if (system("CLS")) system("clear");
			
			if (FIRST_JOYSTICK.Poll())
			{
				std::cout << "POV0: " << FIRST_JOYSTICK.GetPOV(0) << std::endl;
				std::cout << "POV1: " << FIRST_JOYSTICK.GetPOV(1) << std::endl;


				std::cout << "------- " << FIRST_JOYSTICK.Name << " -------" << std::endl;
				std::cout << std::endl;

				//Buttons, you can use up to 128 buttons
				std::cout << "------- " << "Buttons" << " -------" << std::endl;
				for (int i = 0; i < FIRST_JOYSTICK.numberOfButtons; i++)
					std::cout << "Button " << i << ": " << FIRST_JOYSTICK.GetButton(i) << std::endl;

				std::cout << std::endl;

				//Axis, you can use up to 24 buttons
				std::cout << "------- " << "Axis" << " -------" << std::endl;
				for (int i = 0; i < FIRST_JOYSTICK.numberOfAxes; i++)
					std::cout << "Axis " << i << ": " << FIRST_JOYSTICK.GetAxis(i) << std::endl;
				std::cout << std::endl;

				//POV, you can use up to 4 POVs
				for (int pov = 0; pov < FIRST_JOYSTICK.numberOfPOVs; pov++)
				{
					std::cout << "------- " << "POV " << pov << " -------" << std::endl;
					for (int direction = 0; direction < 8; direction++)
					{
						std::cout << "Direction " << direction << ": " << FIRST_JOYSTICK.GetPOVButton(direction, pov) << std::endl;
					}
				}

				std::cout << std::endl;

				//Sliders, you can use up to 8 sliders
				std::cout << "------- " << "Some Sliders" << " -------" << std::endl;
				for (int i = 0; i < 8; i++)
					std::cout << "Slider " << i << ": " << FIRST_JOYSTICK.GetSlider(i) << std::endl;
				std::cout << std::endl;
				std::cout << "-----------------------------------" << std::endl;
			}

			std::this_thread::sleep_for(std::chrono::milliseconds((long)SLEEP_TIME));
		}
	}

	return 0;
}