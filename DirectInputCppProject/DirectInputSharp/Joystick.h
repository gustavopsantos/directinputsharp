#ifndef _JOYSTICK_H_
#define _JOYSTICK_H_

#define DIRECTINPUT_VERSION 0x0800

#include <vector>
#include <dinput.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <string>

class Joystick
{
private:
	LPDIRECTINPUTDEVICE8 const device;

public:
	std::string Name;

	bool Poll();

	//Buttons
	bool GetButton(int button);
	bool GetButtonDown(int button);
	bool GetButtonStay(int button);
	bool GetButtonUp(int button);

	//POVs
	int GetPOV(int povIndex);
	bool GetPOVButton(int button, int povIndex = 0);
	bool GetPOVButtonDown(int button, int povIndex = 0);
	bool GetPOVButtonStay(int button, int povIndex = 0);
	bool GetPOVButtonUp(int button, int povIndex = 0);

	//Axis
	float AxisDeadzones[24];
	float GetAxis(int index);
	float GetLastAxis(int index);
	float GetAxisDelta(int index);

	//Sliders
	float SlidersDeadzones[8];
	float GetSlider(int index);
	float GetLastSlider(int index);
	float GetSliderDelta(int index);

	//Properties
	unsigned int numberOfAxes;
	unsigned int numberOfPOVs;
	unsigned int numberOfButtons;

	//States
	DIJOYSTATE2 currentState;
	DIJOYSTATE2 previousState;

	Joystick(LPDIRECTINPUTDEVICE8 const d);
	~Joystick();
};

#endif