#ifndef _DIRECT_INPUT_SHARP_H
#define _DIRECT_INPUT_SHARP_H

#define DIRECTINPUT_VERSION 0x0800
#define DIRECT_INPUT_SHARP_API __declspec(dllexport)

#include "Joystick.h"
#include "InputHandler.h"

extern "C"
{
	DIRECT_INPUT_SHARP_API InputHandler* Initialize();
	DIRECT_INPUT_SHARP_API int GetConnectedJoysticksCount(InputHandler* inputHandler);
	DIRECT_INPUT_SHARP_API Joystick* GetJoystick(InputHandler* inputHandler, int index);
	DIRECT_INPUT_SHARP_API char* GetJoystickName(Joystick* joystick);
	DIRECT_INPUT_SHARP_API int GetJoystickButtonsCount(Joystick* joystick);
	DIRECT_INPUT_SHARP_API int GetJoystickAxisCount(Joystick* joystick);
	DIRECT_INPUT_SHARP_API int GetJoystickPOVsCount(Joystick* joystick);

	//Polling
	DIRECT_INPUT_SHARP_API bool PollJoystick(Joystick* joystick);

	//Buttons
	DIRECT_INPUT_SHARP_API bool GetButton(Joystick* joystick, int button);
	DIRECT_INPUT_SHARP_API bool GetButtonDown(Joystick* joystick, int button);
	DIRECT_INPUT_SHARP_API bool GetButtonStay(Joystick* joystick, int button);
	DIRECT_INPUT_SHARP_API bool GetButtonUp(Joystick* joystick, int button);

	//Axis
	DIRECT_INPUT_SHARP_API void SetAxisDeadzone(Joystick* joystick, int axis, float deadzone);
	DIRECT_INPUT_SHARP_API float GetAxis(Joystick* joystick, int axis);
	DIRECT_INPUT_SHARP_API float GetLastAxis(Joystick* joystick, int axis);
	DIRECT_INPUT_SHARP_API float GetAxisDelta(Joystick* joystick, int axis);

	//POVs
	DIRECT_INPUT_SHARP_API int GetPOV(Joystick* joystick, int povIndex);
	DIRECT_INPUT_SHARP_API bool GetPOVButton(Joystick* joystick, int button, int povIndex);
	DIRECT_INPUT_SHARP_API bool GetPOVButtonDown(Joystick* joystick, int button, int povIndex);
	DIRECT_INPUT_SHARP_API bool GetPOVButtonStay(Joystick* joystick, int button, int povIndex);
	DIRECT_INPUT_SHARP_API bool GetPOVButtonUp(Joystick* joystick, int button, int povIndex);

	//Sliders
	DIRECT_INPUT_SHARP_API void SetSliderDeadzone(Joystick* joystick, int slider, float deadzone);
	DIRECT_INPUT_SHARP_API float GetSlider(Joystick* joystick, int slider);
	DIRECT_INPUT_SHARP_API float GetLastSlider(Joystick* joystick, int axis);
	DIRECT_INPUT_SHARP_API float GetSliderDelta(Joystick* joystick, int axis);
}
#endif