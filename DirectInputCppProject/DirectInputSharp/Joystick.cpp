#include "Joystick.h"
#include <iostream>

bool Joystick::Poll()
{
	HRESULT hr;

	// store the current state
	CopyMemory(&previousState, &currentState, sizeof(DIJOYSTATE2));
	ZeroMemory(&currentState, sizeof(DIJOYSTATE2));

	// poll the device to read the current state
	hr = device->Poll();

	if (FAILED(hr))
	{
		// DirectInput lost the device, try to re-acquire it
		hr = device->Acquire();
		while (hr == DIERR_INPUTLOST)
			hr = device->Acquire();

		// return if a fatal error is encountered
		if ((hr == DIERR_INVALIDPARAM) || (hr == DIERR_NOTINITIALIZED))
			return E_FAIL;

		// if another application has control of this device, we have to wait for our turn
		if (hr == DIERR_OTHERAPPHASPRIO)
			return S_OK;
	}

	//TODO check if will work in a device with more outputs
	if (FAILED(hr = device->GetDeviceState(sizeof(DIJOYSTATE2), &currentState)))
		if (FAILED(hr = device->GetDeviceState(sizeof(DIJOYSTATE), &currentState)))
			return false;

	return true;
}

Joystick::Joystick(LPDIRECTINPUTDEVICE8 const d) : device(d)
{
	// get game controller name

	DIDEVICEINSTANCE deviceInfo;
	deviceInfo.dwSize = sizeof(DIDEVICEINSTANCE);

	device->GetDeviceInfo(&deviceInfo);
	this->Name = deviceInfo.tszInstanceName;

	// get game controller capabilities
	DIDEVCAPS capabilities;
	//BOOLEAN    VibrationAvailable;
	capabilities.dwSize = sizeof(DIDEVCAPS);
	device->GetCapabilities(&capabilities);

	std::cout << std::endl;

	numberOfAxes = capabilities.dwAxes;
	numberOfPOVs = capabilities.dwPOVs;
	numberOfButtons = capabilities.dwButtons;

	memset(AxisDeadzones, 0, sizeof(float) * 24);

	// clean joystick states
	ZeroMemory(&previousState, sizeof(DIJOYSTATE2));
	ZeroMemory(&currentState, sizeof(DIJOYSTATE2));
}

bool Joystick::GetButton(int button)
{
	return currentState.rgbButtons[button];
}

bool Joystick::GetButtonDown(int button)
{
	return currentState.rgbButtons[button] && !previousState.rgbButtons[button];
}

bool Joystick::GetButtonStay(int button)
{
	return currentState.rgbButtons[button] && previousState.rgbButtons[button];
}

bool Joystick::GetButtonUp(int button)
{
	return !currentState.rgbButtons[button] && previousState.rgbButtons[button];
}

int Joystick::GetPOV(int povIndex)
{
	return currentState.rgdwPOV[povIndex];
}

bool Joystick::GetPOVButton(int button, int povIndex)
{
	switch (button)
	{
	case 0:
		return currentState.rgdwPOV[povIndex] == 0;
	case 1:
		return currentState.rgdwPOV[povIndex] == 4500;
	case 2:
		return currentState.rgdwPOV[povIndex] == 9000;
	case 3:
		return currentState.rgdwPOV[povIndex] == 13500;
	case 4:
		return currentState.rgdwPOV[povIndex] == 18000;
	case 5:
		return currentState.rgdwPOV[povIndex] == 22500;
	case 6:
		return currentState.rgdwPOV[povIndex] == 27000;
	case 7:
		return currentState.rgdwPOV[povIndex] == 31500;
	default:
		return false;
	}
}

bool Joystick::GetPOVButtonDown(int button, int povIndex)
{
	switch (button)
	{
	case 0:
		return currentState.rgdwPOV[povIndex] == 0 && previousState.rgdwPOV[povIndex] != 0;
	case 1:
		return currentState.rgdwPOV[povIndex] == 4500 && previousState.rgdwPOV[povIndex] != 4500;
	case 2:
		return currentState.rgdwPOV[povIndex] == 9000 && previousState.rgdwPOV[povIndex] != 9000;
	case 3:
		return currentState.rgdwPOV[povIndex] == 13500 && previousState.rgdwPOV[povIndex] != 13500;
	case 4:
		return currentState.rgdwPOV[povIndex] == 18000 && previousState.rgdwPOV[povIndex] != 18000;
	case 5:
		return currentState.rgdwPOV[povIndex] == 22500 && previousState.rgdwPOV[povIndex] != 22500;
	case 6:
		return currentState.rgdwPOV[povIndex] == 27000 && previousState.rgdwPOV[povIndex] != 27000;
	case 7:
		return currentState.rgdwPOV[povIndex] == 31500 && previousState.rgdwPOV[povIndex] != 31500;
	default:
		return false;
	}
}

bool Joystick::GetPOVButtonStay(int button, int povIndex)
{
	switch (button)
	{
	case 0:
		return currentState.rgdwPOV[povIndex] == 0 && previousState.rgdwPOV[povIndex] == 0;
	case 1:
		return currentState.rgdwPOV[povIndex] == 4500 && previousState.rgdwPOV[povIndex] == 4500;
	case 2:
		return currentState.rgdwPOV[povIndex] == 9000 && previousState.rgdwPOV[povIndex] == 9000;
	case 3:
		return currentState.rgdwPOV[povIndex] == 13500 && previousState.rgdwPOV[povIndex] == 13500;
	case 4:
		return currentState.rgdwPOV[povIndex] == 18000 && previousState.rgdwPOV[povIndex] == 18000;
	case 5:
		return currentState.rgdwPOV[povIndex] == 22500 && previousState.rgdwPOV[povIndex] == 22500;
	case 6:
		return currentState.rgdwPOV[povIndex] == 27000 && previousState.rgdwPOV[povIndex] == 27000;
	case 7:
		return currentState.rgdwPOV[povIndex] == 31500 && previousState.rgdwPOV[povIndex] == 31500;
	default:
		return false;
	}
}

bool Joystick::GetPOVButtonUp(int button, int povIndex)
{
	switch (button)
	{
	case 0:
		return currentState.rgdwPOV[povIndex] != 0 && previousState.rgdwPOV[povIndex] == 0;
	case 1:
		return currentState.rgdwPOV[povIndex] != 4500 && previousState.rgdwPOV[povIndex] == 4500;
	case 2:
		return currentState.rgdwPOV[povIndex] != 9000 && previousState.rgdwPOV[povIndex] == 9000;
	case 3:
		return currentState.rgdwPOV[povIndex] != 13500 && previousState.rgdwPOV[povIndex] == 13500;
	case 4:
		return currentState.rgdwPOV[povIndex] != 18000 && previousState.rgdwPOV[povIndex] == 18000;
	case 5:
		return currentState.rgdwPOV[povIndex] != 22500 && previousState.rgdwPOV[povIndex] == 22500;
	case 6:
		return currentState.rgdwPOV[povIndex] != 27000 && previousState.rgdwPOV[povIndex] == 27000;
	case 7:
		return currentState.rgdwPOV[povIndex] != 31500 && previousState.rgdwPOV[povIndex] == 31500;
	default:
		return false;
	}
}

float Joystick::GetAxis(int index)
{
	LONG axis[] =
	{
		currentState.lX,
		currentState.lY,
		currentState.lZ,

		currentState.lRx,
		currentState.lRy,
		currentState.lRz,

		currentState.lVZ,
		currentState.lVY,
		currentState.lVZ,

		currentState.lVRx,
		currentState.lVRy,
		currentState.lVRz,

		currentState.lAX,
		currentState.lAY,
		currentState.lAZ,

		currentState.lARx,
		currentState.lARy,
		currentState.lARz,

		currentState.lFX,
		currentState.lFY,
		currentState.lFZ,

		currentState.lFRx,
		currentState.lFRy,
		currentState.lFRz
	};

	float value = axis[index] / 1000.0f;
	return abs(value) > AxisDeadzones[index] ? value : 0;
}

float Joystick::GetLastAxis(int index)
{
	LONG axis[] =
	{
		previousState.lX,
		previousState.lY,
		previousState.lZ,

		previousState.lRx,
		previousState.lRy,
		previousState.lRz,

		previousState.lVZ,
		previousState.lVY,
		previousState.lVZ,

		previousState.lVRx,
		previousState.lVRy,
		previousState.lVRz,

		previousState.lAX,
		previousState.lAY,
		previousState.lAZ,

		previousState.lARx,
		previousState.lARy,
		previousState.lARz,

		previousState.lFX,
		previousState.lFY,
		previousState.lFZ,

		previousState.lFRx,
		previousState.lFRy,
		previousState.lFRz
	};

	float value = axis[index] / 1000.0f;
	return abs(value) > AxisDeadzones[index] ? value : 0;
}

float Joystick::GetAxisDelta(int index)
{
	return GetAxis(index) - GetLastAxis(index);
}

float Joystick::GetSlider(int index)
{
	LONG sliders[] =
	{
		currentState.rglSlider[0],
		currentState.rglSlider[1],

		currentState.rglVSlider[0],
		currentState.rglVSlider[1],

		currentState.rglASlider[0],
		currentState.rglASlider[1],

		currentState.rglFSlider[0],
		currentState.rglFSlider[1]
	};

	float value = sliders[index] / 1000.0f;
	if (value < -1) return 0;

	return abs(value) > SlidersDeadzones[index] ? value : 0;
}

float Joystick::GetLastSlider(int index)
{
	LONG sliders[] =
	{
		previousState.rglSlider[0],
		previousState.rglSlider[1],

		previousState.rglVSlider[0],
		previousState.rglVSlider[1],

		previousState.rglASlider[0],
		previousState.rglASlider[1],

		previousState.rglFSlider[0],
		previousState.rglFSlider[1]
	};

	float value = sliders[index] / 1000.0f;
	if (value < -1) return 0;

	return abs(value) > SlidersDeadzones[index] ? value : 0;
}

float Joystick::GetSliderDelta(int index)
{
	return GetSlider(index) - GetLastSlider(index);
}

Joystick::~Joystick()
{
}
