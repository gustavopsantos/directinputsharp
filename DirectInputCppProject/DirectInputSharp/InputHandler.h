#ifndef _INPUT_HANDLER_H_
#define _INPUT_HANDLER_H_

#include <dinput.h>
#include <vector>
#include <iostream>

#include "Joystick.h"

class InputHandler
{
private:
	LPDIRECTINPUT8 directInput;
	std::vector<LPDIRECTINPUTDEVICE8> gameControllers;

	bool InitializeJoystick(LPDIRECTINPUTDEVICE8 gameController);

	//Callbacks
	BOOL enumerateGameControllers(LPCDIDEVICEINSTANCE devInst);
	static BOOL CALLBACK staticEnumerateGameControllers(LPCDIDEVICEINSTANCE devInst, LPVOID pvRef);
	static BOOL CALLBACK staticSetGameControllerAxesRanges(LPCDIDEVICEOBJECTINSTANCE devObjInst, LPVOID pvRef);

	//Get window context
	HWND GetConsoleHwnd(void);

public:
	std::vector<Joystick> joysticks;

	bool Initialize();

	InputHandler();
	~InputHandler();
};
#endif