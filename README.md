# 🕹️ Direct Input Sharp
## How to update
1. Download DXSDK (Jun 2010) [DXSDK_Jun10.exe](https://www.microsoft.com/en-us/download/details.aspx?id=6812)
2. If you got error `S1023` when installing, execute this two lines on your CMD
```
MsiExec.exe /passive /X{F0C3E5D1-1ADE-321E-8167-68EF0DE699A5}
```
```
MsiExec.exe /passive /X{1D8E6291-B0D5-35EC-8441-6616F567A0F7}
```

3. After installing the June 2010 DirectX SDK, you may then reinstall the most current version of the [Visual C++ 2010 Redistributable Package](https://www.microsoft.com/en-us/download/details.aspx?id=26999)

## How to setup
1. Go to [releases](https://gitlab.com/gustavopsantos/directinputunity/releases) and download the latest version
2. Import `DirectInputSharp.unitypackage` into your project
3. Go to PlayerSettings -> Other Settings
4. Change `Scripting Runtime Version` to `.NET 4.x Equivalent`

## How to use

### Getting a Joystick
```javascript
Joystick joystick = Manager.Instance.Joysticks[0];
```
> Remember to put `using DirectInputSharp;` on top of your code

### Retrieving Joystick Capabilities
```javascript
//How to get how many buttons my joystick have?
int buttonsCount = joystick.ButtonsCount;
```
```javascript
//How to get how many axis my joystick have?
int axisCount = joystick.AxisCount;
```
```javascript
//How to get how many POVs/Directional Pads my joystick have?
int povsCount = joystick.POVsCount;
```
### Retrieving Joystick Output
#####  Buttons
> DirectInputSharp can read up to 128 buttons
```javascript
//How to get current state of an button?
bool isPressed = joystick.GetButton(button: 0);
```
```javascript
//How to know if a button was pressed?
bool wasPressed = joystick.GetButtonDown(button: 0);
```
```javascript
//How to know if a button still pressed?
bool stillPressed = joystick.GetButtonStay(button: 0);
```
```javascript
//How to know if a button was released?
bool wasReleased = joystick.GetButtonUp(button: 0);
```
#####  Axis
> DirectInputSharp can read up to 24 axis
```javascript
//How to get current state of an axis?
float axisCurrentValue = joystick.GetAxis(axis: 0);
```
```javascript
//How to get last state of an axis?
float axisLastValue = joystick.GetLastAxis(axis: 0);
```
```javascript
//How to know how much an axis has changed?
float axisDelta = joystick.GetAxisDelta(axis: 0);
```
> Remember to set axis deadzones to get precise values

#####  POVs / Directional Pads
> DirectInputSharp can read up to 4 POVs
```javascript
//How to get current button pressed in an POV?
POVButton button = joystick.GetPOV(pov: 0);
```
```javascript
//How to know if a POV button is pressed?
bool povUpIsPressed = joystick.GetPOVButton(povButton: POVButton.Up, pov: 0);
```
```javascript
//How to know if a POV button was pressed?
bool povUpWasPressed = joystick.GetPOVButtonDown(povButton: POVButton.Up, pov: 0);
```
```javascript
//How to know if a POV button still pressed?
bool povUpStillPressed = joystick.GetPOVButtonStay(povButton: POVButton.Up, pov: 0);
```
```javascript
//How to know if a POV button was released?
bool povUpWasReleased = joystick.GetPOVButtonUp(povButton: POVButton.Up, pov: 0);
```

#####  Slider
> Some especific devices have this feature, works like an axis
```javascript
//How to get current state of an slider?
float sliderCurrentValue = joystick.GetSlider(0);
```
```javascript
//How to get last state of an slider?
float sliderLastValue = joystick.GetLastSlider(0);
```
```javascript
//How to know how much an slider has changed?
float sliderDelta = joystick.GetSliderDelta(0);
```
> Remember to set slider deadzones to get precise values
### Setting Deadzones
#####  Axis
```javascript
//0.05f = 5% of deadzone
joystick.SetAxisDeadzone(axis: 0, deadzone: 0.05f);
```
#####  Slider
```javascript
//0.05f = 5% of deadzone
joystick.SetSliderDeadzone(slider: 0, deadzone: 0.05f);
```